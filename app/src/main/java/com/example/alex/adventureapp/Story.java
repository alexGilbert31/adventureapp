package com.example.alex.adventureapp;

public class Story {
    private Page[] mPages;

    public Story() {
        mPages = new Page[7];

        mPages[0] = new Page(
                R.drawable.logo,
                "Hi %1$s",
                "Welcome to Pemberton Music Festival %1$s, aka the best weekend of your life.",
                new Choice("Let's go have fun", 2),
                new Choice("Whats is Pemberton?", 1));

        mPages[1] = new Page(
                R.drawable.pemberton,
                "It's a music festival",
                "Join us as we spend 4 glorious days among music, comedy, arts and friends at the foot of awe-inspiring Mount Currie. Together, we’ve created some amazing memories over the years.",
                new Choice("Join us", 2),
                new Choice("Close", 0));

        mPages[2] = new Page(
                R.drawable.show,
                "Plans",
                "What do you want to do today? Today schedule is Snoop Dogg at 19:00 to 20:30, J. Cole at 21:00 to 21:30 and Kendrick Lamar at 22:30 to 24:00 ",
                new Choice("Let's go see shows", 3),
                new Choice("Stay chill at the camping", 5));

        mPages[3] = new Page(
                R.drawable.peoples,
                "Shows time",
               "Let's go see some shows! Since it's 14:00 now, we need to wait a long time before the first show, do you want to go eat or go see some other shows at other stage ",
                new Choice("Go eat", 4),
                new Choice("See shows", 6));

        mPages[4] = new Page(
                R.drawable.battery,
                "Low Battery",
                "Perfect we just eat, it's 15:30 and your phone is almost empty of battery, what do want to do?",
                new Choice("Go charge it", 5),
                new Choice("No problem with that", 6));

        mPages[5] = new Page(
                R.drawable.sleep,
                "Fail",
                "While charging your phone, you felt asleep and you miss all the shows of the day...");
        mPages[5].setFinal(true);


        mPages[6] = new Page(
                R.drawable.girls,
                "Win",
                "You just enjoy Snoop Dogg, J.Cole and Kendrick Lamar shows with beautiful girls, I hope you are ready for a other full day of awesomeness!!");
        mPages[6].setFinal(true);
    }

    public Page getPage (int pageNumber){
        return mPages[pageNumber];
    }
}
