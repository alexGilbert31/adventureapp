package com.example.alex.adventureapp;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class InfoActivity extends AppCompatActivity {

    //For console log with the activity name
    public static final String TAG_ACTIVITY = InfoActivity.class.getSimpleName();

    private String userName;

    private TextView storyContent;
    private TextView title;
    private Button choice_1;
    private Button choice_2;
    private Button restart;
    private Story story;
    private ImageView image;

    private int pageIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme_NoActionBar);
        setContentView(R.layout.activity_info);

        storyContent =  (TextView) findViewById(R.id.storyContent);
        story = new Story();
        choice_1 = (Button) findViewById(R.id.choice1);
        choice_2 = (Button) findViewById(R.id.choice2);
        restart = (Button) findViewById(R.id.restart);
        restart.setVisibility(View.INVISIBLE);
        image = (ImageView) findViewById(R.id.backgroundImage);
        image.setScaleType(ImageView.ScaleType.CENTER_CROP);

        //#8BC34A == 0x ALPHA HEX == 0xFF 8BC34A, FF ==100%, 00 == 0%
        //Set color for the button
        choice_1.getBackground().setColorFilter(0xFF8BC34A, PorterDuff.Mode.MULTIPLY);
        choice_2.getBackground().setColorFilter(0xFFFF9800, PorterDuff.Mode.MULTIPLY);
        restart.getBackground().setColorFilter(0xFF8BC34A, PorterDuff.Mode.MULTIPLY);

        //To add a filter over the image, this case is a to add some black background with 50 of alpha
       // image.setColorFilter(Color.argb(50, 0, 0, 0));

        //get name input by the user in last activity
        Intent in = getIntent();
        userName = CapitalizeFirstLetter(in.getExtras().getString("inputName"));
        String welcomeText = "Hi " + userName;
        //set name input by the user in last activity
        title =  (TextView) findViewById(R.id.titleCard);
        title.setText(welcomeText);

        //Call the first page
        ChangePage(pageIndex);



        choice_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Page page = story.getPage(pageIndex);
                ChangePage( page.getChoice1().getNextPage());
            }

        });

        choice_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Page page = story.getPage(pageIndex);
                ChangePage( page.getChoice2().getNextPage());
            }

        });

        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangePage(0);
            }

        });



    }

    protected void ChangePage (int num)  {
        pageIndex = num;
        Page page = story.getPage(pageIndex);

        image.setImageDrawable(getDrawable(page.getImageId()));
        title.setText(String.format(page.getTitle(), userName));
        storyContent.setText( String.format(page.getText(), userName) );

        //See the logo complete with white around
        //For other image, fill up the card and crop the image
        if (pageIndex ==0) {
            image.setScaleType(ImageView.ScaleType.FIT_CENTER);
        } else {
            image.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }

       //Remove choice button when it's final and put the restart button
        if (!page.isFinal()) {
            choice_1.setVisibility(View.VISIBLE);
            choice_2.setVisibility(View.VISIBLE);
            restart.setVisibility(View.INVISIBLE);
            choice_1.setText(page.getChoice1().getText());
            choice_2.setText(page.getChoice2().getText());

        } else {

            choice_1.setVisibility(View.INVISIBLE);
            choice_2.setVisibility(View.INVISIBLE);
            restart.setVisibility(View.VISIBLE);
        }
    }

    private String CapitalizeFirstLetter (String text) {
        return text.substring(0, 1).toUpperCase() + text.substring(1).toLowerCase();
    }
}
