package com.example.alex.adventureapp;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    //final == const in JS
    public static final String TAG_ACTIVITY = InfoActivity.class.getSimpleName();

    private EditText m_EditText;
    private Button m_button;
    private String input;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme_NoActionBar);
        setContentView(R.layout.activity_main);


        m_EditText = (EditText) findViewById(R.id.editText);
        m_button = (Button) findViewById(R.id.infoActivityButton);

        m_EditText.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);


        m_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input = m_EditText.getText().toString();
                if (input.length() < 1) {
                    //Show a warning toast if no text is enter in the input
                    Toast warning = Toast.makeText(getApplicationContext(), "Enter a name", Toast.LENGTH_SHORT);
                    warning.show();
                    return;

                }
               startInfoActivity(input);
            }

        });

        m_EditText.setOnKeyListener(new View.OnKeyListener()
        {
            public boolean onKey(View v, int keyCode, KeyEvent event)
            {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        input = m_EditText.getText().toString();
                        startInfoActivity(input);
                        return true;
                    }
                }
                return false;
                }


        });

    }

        private void startInfoActivity (String input) {
        Intent secondActivityIntent = new Intent (getApplicationContext(), InfoActivity.class);
        secondActivityIntent.putExtra("inputName", input );
        startActivity(secondActivityIntent);
    }



}
